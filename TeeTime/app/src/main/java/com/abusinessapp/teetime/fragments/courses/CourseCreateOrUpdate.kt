package com.abusinessapp.teetime.fragments.courses

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.abusinessapp.teetime.R
import com.abusinessapp.teetime.fragments.OnPageChangeInterface
import com.abusinessapp.teetime.mainObjects.CourseObject
import com.abusinessapp.teetime.mainObjects.HoleObject
import com.abusinessapp.teetime.mainObjects.PageType
import com.abusinessapp.teetime.viewModel.MainViewModel
import com.abusinessapp.teetime.viewModel.SelectedCourseLiveData
import io.realm.RealmList

class CourseCreateOrUpdate : Fragment(), View.OnClickListener {

    private lateinit var mainView : View
    private lateinit var courseObject : CourseObject
    private var resetStrokes : Boolean = false
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var pageType : PageType
    private var onPageChangeCallback: OnPageChangeInterface? = null

    companion object {
        fun newInstance(bundle: Bundle): CourseCreateOrUpdate {
            val fragment = CourseCreateOrUpdate()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pageType = arguments?.get("KEY_PAGE_TYPE") as PageType
        onPageChangeCallback = try {
            activity as OnPageChangeInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " failed to implement OnPageChangeInterface ")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.course_create_or_update_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.mainView = view
        val addCourseBtn : Button = view.findViewById(R.id.submit_button)
        val cancelBtn : Button = view.findViewById(R.id.cancel_button)
        val resetStrokesBtn : Button = view.findViewById(R.id.reset_button)

        addCourseBtn.setOnClickListener(this)
        cancelBtn.setOnClickListener(this)
        resetStrokesBtn.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getSelectedCourseLiveData().observe(this, Observer { courseObject ->
            courseObject?.let {
                this.courseObject = courseObject
                syncViewWithModel()
            }
        })
    }

    override fun onPause() {
        super.onPause()
        viewModel.getSelectedCourseLiveData().removeObservers(this)
    }

    /**
     * Handles Submit, Cancel, and Resetting oof strokes
     */
    override fun onClick(view: View?) {
        when(view?.id){
            R.id.cancel_button ->{
                when(pageType){
                    PageType.COURSE_NEW ->{
                        onPageChangeCallback?.onPageChangeTrigger(courseObject, PageType.COURSE_LIST)
                    }
                    PageType.COURSE_UPDATE ->{
                        onPageChangeCallback?.onPageChangeTrigger(courseObject, PageType.COURSE_OVERVIEW)
                    }
                    else -> {
                        //Shouldn't ever be here
                    }
                }            }
            R.id.submit_button -> {
                if(resetStrokes){
                    resetStrokes()
                }
                syncModelWithViews()
                val courseData: SelectedCourseLiveData? = viewModel.let { viewModel.getSelectedCourseLiveData() }
                courseData?.save()

                when(pageType){
                    PageType.COURSE_NEW ->{
                        onPageChangeCallback?.onPageChangeTrigger(courseObject, PageType.COURSE_LIST)
                    }
                    PageType.COURSE_UPDATE ->{
                        onPageChangeCallback?.onPageChangeTrigger(courseObject, PageType.COURSE_OVERVIEW)
                    }
                    else -> {
                        //Shouldn't ever be here
                    }
                }
            }
            R.id.reset_button ->{
                if(resetStrokes) {
                    resetStrokes = false
                    syncViewWithModel()
                }else {
                    launchResetAlert()
                }
            }
        }
    }

    /**
     * Syncs the views with the model
     */
    private fun syncViewWithModel(){

        val courseName : EditText = mainView.findViewById(R.id.course_name)
        val coursePar : EditText = mainView.findViewById(R.id.course_par)
        val current : TextView = mainView.findViewById(R.id.current)
        val holes : EditText = mainView.findViewById(R.id.number_of_holes)
        val resetStrokesBtn : Button = mainView.findViewById(R.id.reset_button)
        resetStrokesBtn.text = if (resetStrokes) "Undo?" else "Reset Current Strokes"

        val holesCount = courseObject.holes.size

        var currentTotal = 0

        if(!resetStrokes){
            for (hole in courseObject.holes) {
                currentTotal += hole.current
            }
        }

        courseName.setText(courseObject.name)
        coursePar.setText(courseObject.par.toString())
        current.text = if(currentTotal != 0) currentTotal.toString() else null //show the hint

        holes.setText(holesCount.toString())
    }

    /**
     * Syncs the model with the views
     */
    private fun syncModelWithViews(){
        val courseName : EditText = mainView.findViewById(R.id.course_name)
        val coursePar : EditText = mainView.findViewById(R.id.course_par)
        val holes : EditText = mainView.findViewById(R.id.number_of_holes)

        //Only update the holes if they have changed
        val numberOfHoles = if (holes.text.isEmpty()) 0 else holes.text.toString().toInt()
        if(numberOfHoles != courseObject.holes.size) {
            val holeList: RealmList<HoleObject> = RealmList()
            for (i in 0 until numberOfHoles) {
                val hole = HoleObject()
                hole.number = i + 1
                //re-assign the current strokes
                if (i < courseObject.holes.size) {
                    hole.current = courseObject.holes[i]!!.current
                }
                holeList.add(hole)
            }
            courseObject.holes = holeList
        }

        courseObject.name = courseName.text.toString()
        courseObject.par = if (coursePar.text.isEmpty()) 0 else coursePar.text.toString().toInt()
    }

    /**
     * ResetStrokes: alert dialog
     */
    private fun launchResetAlert(){
        val alert = AlertDialog.Builder(activity)
        alert.setTitle("Reset your Current Strokes?")
        alert.setPositiveButton("Reset"){ dialog, _ ->
            resetStrokes = true
            syncViewWithModel()
            dialog.dismiss()
        }
        alert.setNegativeButton("Cancel"){ dialog, _ ->
            dialog.dismiss()
        }

        alert.create().show()
    }

    /**
     * Helper to reset the strokes on the object
     */
    private fun resetStrokes(){
        for (hole in courseObject.holes){
            hole.current = 0
        }
    }
}