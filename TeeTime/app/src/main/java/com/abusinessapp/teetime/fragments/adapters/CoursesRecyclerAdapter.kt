package com.abusinessapp.teetime.fragments.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abusinessapp.teetime.R
import com.abusinessapp.teetime.inflate
import com.abusinessapp.teetime.mainObjects.CourseObject
import com.abusinessapp.teetime.mainObjects.HoleObject
import kotlinx.android.synthetic.main.courses_recycler_row_item.view.*

class CoursesRecyclerAdapter(private var courses: MutableList<CourseObject>, private val itemClickListener: (View, Int) -> Unit) : RecyclerView.Adapter<CoursesRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.courses_recycler_row_item, false)
        return ViewHolder(
            inflatedView
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val course = courses[position]
        val holes : MutableList<HoleObject> = course.holes

        var currentTotal = 0

        for (hole in holes){
            currentTotal += hole.current
        }

        holder.itemView.course_name.text = course.name
        holder.itemView.course_holes.text = holes.size.toString()
        holder.itemView.course_par.text = course.par.toString()
        holder.itemView.course_current_score.text = currentTotal.toString()
        holder.itemView.setOnClickListener{
            itemClickListener.invoke(holder.itemView, position)
        }
    }

    override fun getItemCount(): Int {
        return courses.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
