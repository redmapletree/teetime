package com.abusinessapp.teetime.mainObjects

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class CourseObject (
    @PrimaryKey
    var id : String = UUID.randomUUID().toString(),
    var name : String = "My New Course",
    var par : Int = 0,
    var holes : RealmList<HoleObject> = RealmList()
) : RealmObject()



