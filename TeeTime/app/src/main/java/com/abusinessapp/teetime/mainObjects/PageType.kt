package com.abusinessapp.teetime.mainObjects

enum class PageType {
    COURSE_LIST,
    COURSE_OVERVIEW,
    COURSE_NEW,
    COURSE_UPDATE,
    HOLE_UPDATE
}