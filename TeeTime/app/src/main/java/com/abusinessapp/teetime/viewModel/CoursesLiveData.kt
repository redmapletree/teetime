package com.abusinessapp.teetime.viewModel

import androidx.lifecycle.LiveData
import com.abusinessapp.teetime.backEnd.RealmManager
import com.abusinessapp.teetime.mainObjects.CourseObject
import io.realm.RealmList
import io.realm.RealmResults
import io.realm.kotlin.where

class CoursesLiveData : LiveData<RealmList<CourseObject>>() {

        private val realmManager = RealmManager()

        override fun onActive() {
            realmManager.openLocalInstance()
            val result : RealmResults<CourseObject> = realmManager.localInstance.where<CourseObject>().findAll()
            val list : RealmList<CourseObject> = RealmList()
            list.addAll(realmManager.localInstance.copyFromRealm(result).subList(0, realmManager.localInstance.copyFromRealm(result).size))
            value = list
            realmManager.closeLocalInstance()
        }

        override fun onInactive() {
        }

        fun set(courses: RealmList<CourseObject> ){
            value = courses
        }
}
