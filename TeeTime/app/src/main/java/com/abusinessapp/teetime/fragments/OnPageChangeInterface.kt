package com.abusinessapp.teetime.fragments

import com.abusinessapp.teetime.mainObjects.PageType

interface OnPageChangeInterface {
    fun onPageChangeTrigger(any: Any, pageType : PageType)
}