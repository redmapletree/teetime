package com.abusinessapp.teetime.backEnd

import io.realm.Realm
import io.realm.RealmConfiguration


class RealmManager internal constructor() {

    private val localRealms: ThreadLocal<Realm?> = ThreadLocal<Realm?>()

    /**
     * Returns the local Realm instance without adding to the reference count.
     *
     * @return the local Realm instance
     * @throws IllegalStateException when no Realm is open
     */
    val localInstance: Realm
        get() = localRealms.get()
            ?: throw IllegalStateException(
                "No open Realms were found on this thread."
            )

    /**
     * Opens a reference-counted local Realm instance.
     *
     * @return the open Realm instance
     */
    fun openLocalInstance(): Realm {
        checkDefaultConfiguration()
        val realm: Realm = Realm.getDefaultInstance()
        if (localRealms.get() == null) {
            localRealms.set(realm)
        }
        return realm
    }

    /**
     * Closes local Realm instance, decrementing the reference count.
     *
     * @throws IllegalStateException if there is no open Realm.
     */
    fun closeLocalInstance() {
        checkDefaultConfiguration()
        val realm: Realm = localRealms.get()
            ?: throw IllegalStateException(
                "No Realms are open open."
            )
        realm.close()
        // noinspection ConstantConditions
        val  defaultConfig : RealmConfiguration? = Realm.getDefaultConfiguration()
        if(defaultConfig!=null) {
            if (Realm.getDefaultConfiguration() != null && Realm.getLocalInstanceCount(defaultConfig) <= 0) {
                localRealms.set(null)
            }
        }
    }

    private fun checkDefaultConfiguration() {
        checkNotNull(Realm.getDefaultConfiguration()) { "No default configuration is set." }
    }
}