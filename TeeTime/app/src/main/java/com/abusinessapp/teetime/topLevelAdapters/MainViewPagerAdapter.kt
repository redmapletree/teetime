package com.abusinessapp.teetime.topLevelAdapters

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter


class MainViewPagerAdapter(fragmentManager : FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
{

    private val fragments : MutableList<Fragment> = ArrayList()
    private val pageTitles : MutableList<String> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getItemPosition(`object`: Any): Int {
        if(fragments.contains(`object`)){
            return fragments.indexOf(`object`)
        }
        return POSITION_NONE
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return pageTitles[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun addFragment(fragment: Fragment, pageTitle :String){
        fragments.add(fragment)
        pageTitles.add(pageTitle)
    }

    fun removeFragment(fragmentPosition: Int){
        pageTitles.removeAt(fragmentPosition)
        fragments.removeAt(fragmentPosition)
        notifyDataSetChanged()
    }
}