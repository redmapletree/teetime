package com.abusinessapp.teetime.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abusinessapp.teetime.mainObjects.CourseObject
import com.abusinessapp.teetime.mainObjects.HoleObject

class MainViewModel : ViewModel() {

    private val selectedCourseLiveData = SelectedCourseLiveData()
    private val coursesLiveData = CoursesLiveData()
    private val holeLiveData = MutableLiveData<HoleObject>()

    fun getSelectedCourseLiveData(): SelectedCourseLiveData {
        return selectedCourseLiveData
    }

    fun setSelectedCourseLiveData(courseObject: CourseObject){
        selectedCourseLiveData.set(courseObject)
    }

    fun getCoursesLiveData() : CoursesLiveData{
        return coursesLiveData
    }

    fun getSelectedHoleLiveData(): MutableLiveData<HoleObject> {
        return holeLiveData
    }

    fun setSelectedHoleLiveData(holeObject: HoleObject){
        holeLiveData.value = (holeObject)
    }
}