package com.abusinessapp.teetime

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.abusinessapp.teetime.fragments.OnPageChangeInterface
import com.abusinessapp.teetime.fragments.courses.CourseCreateOrUpdate
import com.abusinessapp.teetime.fragments.courses.CourseListFragment
import com.abusinessapp.teetime.fragments.courses.CourseOverviewFragment
import com.abusinessapp.teetime.fragments.holes.HoleCreateOrUpdate
import com.abusinessapp.teetime.mainObjects.CourseObject
import com.abusinessapp.teetime.mainObjects.HoleObject
import com.abusinessapp.teetime.mainObjects.PageType
import com.abusinessapp.teetime.topLevelAdapters.MainViewPagerAdapter
import com.abusinessapp.teetime.viewModel.MainViewModel
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity(), OnPageChangeInterface {

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPagerAdapter : MainViewPagerAdapter
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAdapter()
    }

    private fun initAdapter(){
        viewPager = findViewById(R.id.main_view_pager)
        tabLayout = findViewById(R.id.main_tab_layout)
        viewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = viewPagerAdapter
        //Add the courses fragment when setting up
        viewPagerAdapter.addFragment(CourseListFragment(), "My Courses")
        viewPagerAdapter.notifyDataSetChanged()
        //refresh the tab layout
        tabLayout.setupWithViewPager(viewPager)
    }

    /** handles the back button
     * If the viewpager has more than one fragment remove one
     */
    override fun onBackPressed() {
        if(viewPager.currentItem > 0){
            viewPager.currentItem = viewPager.currentItem-1
            viewPagerAdapter.removeFragment(viewPagerAdapter.count - 1)
            return
        }
        super.onBackPressed()
    }

    /**
     * This handles the viewPagers stack
     */
    override fun onPageChangeTrigger(any: Any, pageType: PageType) {
        //Grab the object
        var courseObject = CourseObject()
        var holeObject = HoleObject()
        when(any){
            is CourseObject ->
                courseObject = any
            is HoleObject ->
                holeObject = any
        }

        val fragment : Fragment
        val tabTitle : String

        val bundle = bundleOf (
            "KEY_PAGE_TYPE" to pageType
        )

        when (pageType) {
            PageType.COURSE_LIST -> {
                fragment = CourseListFragment.newInstance(bundle)
                tabTitle = "My Courses"
                //Remove all frags aside from courseList
                for (x in viewPagerAdapter.count - 1 downTo 1 step 1) {
                    viewPagerAdapter.removeFragment(x)
                }
            }
            PageType.COURSE_OVERVIEW -> {
                fragment = CourseOverviewFragment.newInstance(bundle)
                tabTitle = "Overview"
                //Remove all frags aside from courseList
                for (x in viewPagerAdapter.count - 1 downTo 1 step 1) {
                    viewPagerAdapter.removeFragment(x)
                }
                viewModel.setSelectedCourseLiveData(courseObject)
            }
            PageType.COURSE_NEW -> {
                fragment = CourseCreateOrUpdate.newInstance(bundle)
                tabTitle = "New Course"
                //Remove all frags aside from courseList
                for (x in viewPagerAdapter.count - 1 downTo 1 step 1) {
                    viewPagerAdapter.removeFragment(x)
                }
                viewModel.setSelectedCourseLiveData(courseObject)
            }
            PageType.COURSE_UPDATE -> {
                fragment = CourseCreateOrUpdate.newInstance(bundle)
                tabTitle = "Update Course"
                //Remove all frags aside from courseList and selected course overview
                for (x in viewPagerAdapter.count - 1 downTo 2 step 1) {
                    viewPagerAdapter.removeFragment(x)
                }
                viewModel.setSelectedCourseLiveData(courseObject)
            }
            PageType.HOLE_UPDATE -> {
                viewModel.setSelectedHoleLiveData(holeObject)
                fragment = HoleCreateOrUpdate.newInstance(bundle)
                tabTitle = "Update Hole"
                //Remove all frags aside from courseList and selected course
                for (x in viewPagerAdapter.count - 1 downTo 2 step 1) {
                    viewPagerAdapter.removeFragment(x)
                }
            }
        }

        viewPagerAdapter.addFragment(fragment, tabTitle)
        viewPagerAdapter.notifyDataSetChanged()
        viewPager.currentItem = viewPagerAdapter.count-1
    }
}