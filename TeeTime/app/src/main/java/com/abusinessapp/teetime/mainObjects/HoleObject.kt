package com.abusinessapp.teetime.mainObjects

import io.realm.RealmObject

open class HoleObject (
    var number : Int = 0,
    var par : Int = 0,
    var current : Int = 0
) : RealmObject()