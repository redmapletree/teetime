package com.abusinessapp.teetime.fragments.courses

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abusinessapp.teetime.R
import com.abusinessapp.teetime.fragments.OnPageChangeInterface
import com.abusinessapp.teetime.fragments.adapters.HolesRecyclerAdapter
import com.abusinessapp.teetime.fragments.holes.HoleCreateOrUpdate
import com.abusinessapp.teetime.mainObjects.CourseObject
import com.abusinessapp.teetime.mainObjects.HoleObject
import com.abusinessapp.teetime.mainObjects.PageType
import com.abusinessapp.teetime.viewModel.MainViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.realm.RealmList

class CourseOverviewFragment : Fragment(), (View, Int) -> Unit,
    View.OnClickListener {

    private lateinit var mainView : View
    private lateinit var holesRecyclerAdapter: HolesRecyclerAdapter
    private lateinit var holesRecycler: RecyclerView
    private var holes: RealmList<HoleObject> = RealmList()
    private lateinit var courseObject : CourseObject
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var pageType : PageType
    private var onPageChangeCallback: OnPageChangeInterface? = null

    companion object {
        fun newInstance(bundle: Bundle): CourseOverviewFragment {
            val fragment = CourseOverviewFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pageType = arguments?.get("KEY_PAGE_TYPE") as PageType
        onPageChangeCallback = try {
            activity as OnPageChangeInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " failed to implement OnPageChangeInterface ")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.course_overview_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.mainView = view
        val addCourseBtn : FloatingActionButton = view.findViewById(R.id.update_course_button)
        addCourseBtn.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        viewModel.getSelectedCourseLiveData().observe(this, Observer { courseObject ->
            courseObject?.let {
                this.courseObject = courseObject
                updateFields(courseObject)
            }
        })

        setUpRecyclerView()
    }

    override fun onPause() {
        super.onPause()
        viewModel.getSelectedCourseLiveData().save()
        viewModel.getSelectedCourseLiveData().removeObservers(this)
    }

    private fun setUpRecyclerView(){
        holesRecycler = mainView.findViewById(R.id.holes_recycler)
        holesRecycler.layoutManager = LinearLayoutManager(this.context)
        holesRecyclerAdapter =
            HolesRecyclerAdapter(
                holes,
                this
            )
        holesRecycler.adapter = holesRecyclerAdapter
        holesRecyclerAdapter.notifyDataSetChanged()
    }

    private fun updateFields(courseObject : CourseObject){
        val courseName : TextView = mainView.findViewById(R.id.course_name)
        val coursePar : TextView = mainView.findViewById(R.id.course_par)
        val current : TextView = mainView.findViewById(R.id.current)

        holes = courseObject.holes

        var currentTotal = 0

        for (hole in holes){
            currentTotal += hole.current
        }

        courseName.text = courseObject.name
        coursePar.text = courseObject.par.toString()
        current.text = currentTotal.toString()

        if(this::holesRecyclerAdapter.isInitialized) {
            holesRecyclerAdapter.notifyDataSetChanged()
        }
    }

    override fun invoke(p1: View, p2: Int) {
        holes[p2]?.let {
            when (p1.id){
                R.id.increment ->{
                    holes[p2]!!.current += 1
                    holesRecyclerAdapter.notifyItemChanged(p2)
                    updateFields(courseObject)
                }
                R.id.decrement ->{
                    if (holes[p2]!!.current > 0) {
                        holes[p2]!!.current -= 1
                        holesRecyclerAdapter.notifyItemChanged(p2)
                        updateFields(courseObject)
                    }
                }
                R.id.edit_hole_fab ->{
                    launchEditHoleFragment(it)
                }
            }
        }
    }

    override fun onClick(p0: View?) {
        launchEditCourseFragment(courseObject)
    }

    private fun launchEditCourseFragment(courseObject: CourseObject){
        onPageChangeCallback?.onPageChangeTrigger(courseObject, PageType.COURSE_UPDATE)
    }

    private fun launchEditHoleFragment(holeObject: HoleObject){
        onPageChangeCallback?.onPageChangeTrigger(holeObject, PageType.HOLE_UPDATE)
    }
}