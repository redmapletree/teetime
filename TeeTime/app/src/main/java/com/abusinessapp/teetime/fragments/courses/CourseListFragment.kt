package com.abusinessapp.teetime.fragments.courses

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abusinessapp.teetime.R
import com.abusinessapp.teetime.fragments.OnPageChangeInterface
import com.abusinessapp.teetime.fragments.adapters.CoursesRecyclerAdapter
import com.abusinessapp.teetime.mainObjects.CourseObject
import com.abusinessapp.teetime.mainObjects.PageType
import com.abusinessapp.teetime.viewModel.MainViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.realm.RealmList


class CourseListFragment : Fragment(), (View, Int) -> Unit,
    View.OnClickListener {

    private lateinit var coursesRecyclerAdapter: CoursesRecyclerAdapter
    private lateinit var coursesRecycler: RecyclerView
    private var courses: RealmList<CourseObject> = RealmList()
    private val viewModel: MainViewModel by activityViewModels()

    private var onPageChangeCallback: OnPageChangeInterface? = null

    companion object {
        fun newInstance(bundle: Bundle): CourseListFragment {
            val fragment = CourseListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onPageChangeCallback = try {
            activity as OnPageChangeInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " failed to implement OnPageChangeInterface ")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.courses_list_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val addCourseBtn : FloatingActionButton = view.findViewById(R.id.add_course_button)
        addCourseBtn.setOnClickListener(this)

        coursesRecycler = view.findViewById(R.id.courses_recycler)
    }

    override fun onResume() {
        super.onResume()

        viewModel.getCoursesLiveData().observe(this, Observer { courseList ->
            courseList?.let {
                courses = courseList
                if(this::coursesRecyclerAdapter.isInitialized) {
                    coursesRecyclerAdapter.notifyDataSetChanged()
                }
            }
        })

        initRecyclerView()
    }

    override fun onPause() {
        super.onPause()
        viewModel.getCoursesLiveData().removeObservers(this)
    }

    /**
     * Handles adding a course click
     */
    override fun onClick(view: View?) {
        when(view?.id){
            R.id.add_course_button -> launchNewCourseFragment()
        }
    }

    private fun launchNewCourseFragment(){
        onPageChangeCallback?.onPageChangeTrigger(CourseObject(), PageType.COURSE_NEW)
    }

    /**
     * Handles click from recyclerView
     */
    override fun invoke(view: View, position: Int) {
        //Launch the "holes" tab in the viewpager for the selected course
        courses[position]?.let { onPageChangeCallback?.onPageChangeTrigger(it, PageType.COURSE_OVERVIEW) }
    }

    /**
     * Init the RecyclerView
     */
    private fun initRecyclerView(){
        coursesRecycler.layoutManager = LinearLayoutManager(this.context)
        coursesRecyclerAdapter =
            CoursesRecyclerAdapter(
                courses,
                this
            )
        coursesRecycler.adapter = coursesRecyclerAdapter
    }
}

