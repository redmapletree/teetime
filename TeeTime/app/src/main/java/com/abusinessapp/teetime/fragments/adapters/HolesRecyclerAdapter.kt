package com.abusinessapp.teetime.fragments.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abusinessapp.teetime.R
import com.abusinessapp.teetime.inflate
import com.abusinessapp.teetime.mainObjects.HoleObject
import kotlinx.android.synthetic.main.holes_recycler_row_item.view.*

class HolesRecyclerAdapter (private var holes: List<HoleObject>, private val itemClickListener: (View, Int) -> Unit) : RecyclerView.Adapter<HolesRecyclerAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.holes_recycler_row_item, false)
        return ViewHolder(
            inflatedView
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hole = holes[position]

        holder.itemView.hole_name.text = hole.number.toString()
        holder.itemView.hole_par.text = hole.par.toString()
        holder.itemView.hole_current_score.text = hole.current.toString()

        holder.itemView.edit_hole_fab.setOnClickListener{
            itemClickListener.invoke(holder.itemView.edit_hole_fab, position)
        }

        holder.itemView.increment.setOnClickListener {
            itemClickListener.invoke(holder.itemView.increment, position)
        }

        holder.itemView.decrement.setOnClickListener {
            itemClickListener.invoke(holder.itemView.decrement, position)
        }
    }

    override fun getItemCount(): Int {
        return holes.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}