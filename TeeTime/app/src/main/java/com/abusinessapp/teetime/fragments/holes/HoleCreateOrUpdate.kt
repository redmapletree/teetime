package com.abusinessapp.teetime.fragments.holes

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.abusinessapp.teetime.R
import com.abusinessapp.teetime.fragments.OnPageChangeInterface
import com.abusinessapp.teetime.mainObjects.HoleObject
import com.abusinessapp.teetime.mainObjects.PageType
import com.abusinessapp.teetime.viewModel.MainViewModel
import com.abusinessapp.teetime.viewModel.SelectedCourseLiveData

class HoleCreateOrUpdate : Fragment(), View.OnClickListener {

    private lateinit var mainView : View
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var pageType : PageType
    private lateinit var holeObject : HoleObject
    private var onPageChangeCallback: OnPageChangeInterface? = null

    companion object {
        fun newInstance(bundle: Bundle): HoleCreateOrUpdate {
            val fragment = HoleCreateOrUpdate()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pageType = arguments?.get("KEY_PAGE_TYPE") as PageType
        onPageChangeCallback = try {
            activity as OnPageChangeInterface
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " failed to implement OnPageChangeInterface ")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.hole_create_or_update_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.mainView = view
        val addCourseBtn : Button = view.findViewById(R.id.submit_button)
        val cancelBtn : Button = view.findViewById(R.id.cancel_button)
        addCourseBtn.setOnClickListener(this)
        cancelBtn.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getSelectedHoleLiveData().observe(this, Observer { holeObject ->
            this.holeObject =holeObject
            syncViewsWithModel()
        })
    }

    override fun onClick(view: View?) {

        syncModelWithViews()

        val courseData: SelectedCourseLiveData? = viewModel.let { viewModel.getSelectedCourseLiveData() }

        when(view?.id){
            R.id.cancel_button ->{
                courseData?.value?.let { onPageChangeCallback?.onPageChangeTrigger(it, PageType.COURSE_OVERVIEW)
                }
            }
            R.id.submit_button -> {
                courseData?.value?.holes?.set(holeObject.number -1, holeObject)
                courseData?.value?.let {
                    onPageChangeCallback?.onPageChangeTrigger(it, PageType.COURSE_OVERVIEW)
                }
            }
        }
    }

    private fun syncViewsWithModel(){
        val holeName : TextView = mainView.findViewById(R.id.hole_number)
        val holePar : EditText = mainView.findViewById(R.id.par)

        holeName.text = holeObject.number.toString()
        holePar.setText(holeObject.par.toString())
    }

    private fun syncModelWithViews(){
        val holePar : EditText = mainView.findViewById(R.id.par)
        holeObject.par = if (holePar.text.isEmpty()) 0 else holePar.text.toString().toInt()
    }
}