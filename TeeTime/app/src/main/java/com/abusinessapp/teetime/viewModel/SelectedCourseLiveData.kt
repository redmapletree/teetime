package com.abusinessapp.teetime.viewModel

import androidx.lifecycle.LiveData
import com.abusinessapp.teetime.backEnd.RealmManager
import com.abusinessapp.teetime.mainObjects.CourseObject

/**
 * Live data for the course object that is selected.
 * Also contains a save method for the course
 */
class SelectedCourseLiveData: LiveData<CourseObject>() {

    private val realmManager = RealmManager()

    override fun onActive() {
    }

    override fun onInactive() {
    }

    fun set(course: CourseObject){
        value = course
    }

    fun save(){
        if(value!=null) {
            realmManager.openLocalInstance()
            realmManager.localInstance.beginTransaction()
            val course : CourseObject = value as CourseObject
            realmManager.localInstance.copyToRealmOrUpdate(course)
            realmManager.localInstance.commitTransaction()
            realmManager.closeLocalInstance()
        }
    }



    //If ever needed to listen for updates

    //Add to set value
//    realmManager.openLocalInstance()
//        val results = realmManager.localInstance.where(CourseObject::class.java).equalTo("id", course.id).findFirst()
//        value = if(results != null){
//            results.addChangeListener(listener)
//            realmManager.localInstance.copyFromRealm(results)
//        }else{
//            course
//        }
//        realmManager.closeLocalInstance()

    //Just un comment
//    private val listener = { course: CourseObject ->
//        value = realmManager.localInstance.copyFromRealm(course)
//    }
}